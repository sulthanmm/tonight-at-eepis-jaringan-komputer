﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Runtime.InteropServices;
using System;

public class ghostButton : MonoBehaviour
{
    private GameObject data;
    public static string role2 = "a";
    public static Client clients;
    byte[] buffer;
    static PacketData data2send;

    private void Start()
    {
        data = GameObject.FindGameObjectWithTag("RoleManager");
        GameObject go = GameObject.Find("ServerHandler");
        clients = go.GetComponent<Client>();
    }

    private void Update()
    {

    }

    public void SetGhost()
    {
        buffer = getBytes(data2send);
        clients.SendTo(buffer, clients.client);
        role2 = "Ghost";
        data.GetComponent<roleData>().role = "Ghost";
    }

    public struct PacketData
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string role;
        public string process;
        public int num;
        public float Xpos;
        public float Ypos;
        public float Zpos;
        public float Xrot;
        public float Yrot;
        public float Zrot;
    }

    byte[] getBytes(PacketData str)
    {
        int size = Marshal.SizeOf(str);
        byte[] arr = new byte[size];

        IntPtr ptr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(str, ptr, true);
        Marshal.Copy(ptr, arr, 0, size);
        Marshal.FreeHGlobal(ptr);
        return arr;
    }
}
