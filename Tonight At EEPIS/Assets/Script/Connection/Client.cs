﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.IO;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Globalization;
using UnityEngine.AI;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.Runtime.InteropServices;

public class Client : MonoBehaviour
{   
    private string password;
    private bool socketReady = false;
    private int port = 8020;
    private int port2 = 8030;
    private CultureInfo culture = (CultureInfo)CultureInfo.CurrentCulture.Clone();
    private playerMovement player;
    private ghostMovement ghost;
    public UdpClient client;
    private GameObject role;
    static PacketData data2send;
    byte[] buffer;

    // Start is called before the first frame update
    void Start()
    {
        role = GameObject.FindGameObjectWithTag("RoleManager");
        culture.NumberFormat.NumberDecimalSeparator = ".";

        /*
        GameObject pl = GameObject.Find("Player");
        player = pl.GetComponent<playerMovement>();
        GameObject go = GameObject.Find("Enemy");
        ghost = go.GetComponent<ghostMovement>();
        */

        data2send = new PacketData();
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame   
    void Update()
    {        
        
    }

    public void SendTo(byte[] dat, UdpClient client)
    {
        if (!socketReady)
        {
            return;
        }

        IPAddress multicastAddress = IPAddress.Parse("239.0.0.222");
        IPEndPoint sender = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 12345);        
        client.Send(dat, dat.Length, sender);        
    }
    
    public bool ConnectToServer(string host, int port)
    {
        if (socketReady)
            return false;

        try
        {            
            IPAddress multicastAddress = IPAddress.Parse("239.0.0.222");
            client.JoinMulticastGroup(multicastAddress);            
            socketReady = true;
            //SendTo("start", client);
            Debug.Log("Start!!!");
        }
        catch (Exception e)
        {
            Debug.Log("Socket error " + e.Message);
        }

        return socketReady;
    }

    public void ConnectToServerButton()
    {
        try
        {
            client = new UdpClient(port);            
            ConnectToServer("127.0.0.1", port);
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    public void HumanConnect()
    {
        //role.GetComponent<roleData>().role = "Human";
        data2send.role = "Human";
        buffer = getBytes(data2send);
        try
        {
            client = new UdpClient(port2);
            ConnectToServer("127.0.0.1", port);
            SendTo(buffer, client);
            Debug.Log("HumanSelected");
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }

    }

    public void GhostConnect()
    {
        role.GetComponent<roleData>().role = "Ghost";
        data2send.role = "Ghost";
        buffer = getBytes(data2send);       
        try
        {
            client = new UdpClient(port);
            ConnectToServer("127.0.0.1", port);
            SendTo(buffer, client);
            Debug.Log("GhostSelected");          
        }
        catch (Exception e)
        {
            Debug.Log(e.Message);
        }
    }

    public struct PacketData
    {
        [MarshalAs(UnmanagedType.ByValTStr, SizeConst = 100)]
        public string role;
        public string process;
        public int num;
        public float Xpos;
        public float Ypos;
        public float Zpos;
        public float Xrot;
        public float Yrot;
        public float Zrot;
    }

    byte[] getBytes(PacketData str)
    {
        int size = Marshal.SizeOf(str);
        byte[] arr = new byte[size];

        IntPtr ptr = Marshal.AllocHGlobal(size);
        Marshal.StructureToPtr(str, ptr, true);
        Marshal.Copy(ptr, arr, 0, size);
        Marshal.FreeHGlobal(ptr);
        return arr;
    }
}
