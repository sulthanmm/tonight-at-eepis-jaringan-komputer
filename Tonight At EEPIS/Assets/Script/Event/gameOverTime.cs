﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gameOverTime : MonoBehaviour
{
    private int timer = 500;

    void Start()
    {
        StartCoroutine("Countdown");
    }

    // Update is called once per frame
    void Update()
    {
        if (timer == 0)
        {
            // Game Over. Player Win.
        }  
    }

    IEnumerator Countdown()
    {
        timer--;
        yield return new WaitForSecondsRealtime(1f);
        StartCoroutine("Countdown");
    }
}
