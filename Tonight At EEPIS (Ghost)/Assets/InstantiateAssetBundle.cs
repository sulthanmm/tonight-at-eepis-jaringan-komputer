﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using UnityEngine;

public class InstantiateAssetBundle : MonoBehaviour
{
    AssetBundle myLoadBundle;
    public string url;

    public GameObject [] d3;
    public GameObject [] pagar;

    private float []posX = { 0,0,0,0,0,0,0,0,0,0,0,0}; 
    private float []posY = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    private float []posZ = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    private float []rotY = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    private float []scaleX = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    private float []scaleY = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    private float []scaleZ = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    public int counter = 0;

    // Start is called before the first frame update
    void Start()
    {
        d3 = GameObject.FindGameObjectsWithTag("D3Building");
        pagar = GameObject.FindGameObjectsWithTag("Pagar");

        foreach(GameObject building in d3)
        {
            posX[counter] = building.transform.position.x;
            posY[counter] = building.transform.position.y;
            posZ[counter] = building.transform.position.z;

            rotY[counter] = building.transform.localEulerAngles.y;

            scaleX[counter] = building.transform.localScale.x;
            scaleY[counter] = building.transform.localScale.y;
            scaleZ[counter] = building.transform.localScale.z;
            counter++;
        }
        foreach (GameObject building in pagar)
        {
            posX[counter] = building.transform.position.x;
            posY[counter] = building.transform.position.y;
            posZ[counter] = building.transform.position.z;

            rotY[counter] = building.transform.localEulerAngles.y;

            scaleX[counter] = building.transform.localScale.x;
            scaleY[counter] = building.transform.localScale.y;
            scaleZ[counter] = building.transform.localScale.z;
            counter++;
        }
        counter--;
        StartCoroutine("GetAssetBundle");
    }

  /*  void LoadAssetBundle(string bundleUrl)
    {
        myLoadBundle = AssetBundle.LoadFromFile(bundleUrl);
        Debug.Log(myLoadBundle == null ? " Failed " : "AssetBundle loaded");
    }*/

   /* void InstantiateObject(string assetName)
    {
        var prefab = myLoadBundle.LoadAsset(assetName);
        Instantiate(prefab);
    }*/
    IEnumerator GetAssetBundle()
    {
        WWW www = new WWW(url);
     
        yield return www;
        myLoadBundle = www.assetBundle;
        if (www.error == null)
        {
            for(int i=0; i <= counter-4; i++)
            {
                GameObject prefab = (GameObject)myLoadBundle.LoadAsset("D3_Tengah");
                prefab.transform.localScale = new Vector3(scaleX[i], scaleY[i], scaleZ[i]);
                prefab.transform.localEulerAngles = new Vector3(0, rotY[i], 0);
                Instantiate(prefab, new Vector3(posX[i], posY[i], posZ[i]),Quaternion.Euler(prefab.transform.localEulerAngles));
            }
            for(int i=counter-3; i<= counter; i++)
            {
                GameObject prefab = (GameObject)myLoadBundle.LoadAsset("pager_Parkiran");
                prefab.transform.localScale = new Vector3(scaleX[i], scaleY[i], scaleZ[i]);
                prefab.transform.localEulerAngles = new Vector3(0, rotY[i], 0);
                Instantiate(prefab, new Vector3(posX[i], posY[i], posZ[i]), Quaternion.Euler(prefab.transform.localEulerAngles));
            }
            

           
            
        }
        
     
        
    }
}
